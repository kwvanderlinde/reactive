package com.wobble.time;

/**
 * A time is a virtual representation of a real change in time. The reference
 * time is unspecified here. Instead, each application may have its own fixed
 * reference time.
 * 
 * @author ken
 */
public class Time implements Comparable<Time> {
	private final long milliseconds;

	public Time() {
		this(0L);
	}

	public Time(long milliseconds) {
		this.milliseconds = milliseconds;
	}

	public long getMillisecondCount() {
		return this.milliseconds;
	}
	
	@Override
	public int hashCode() {
		return Long.hashCode(this.milliseconds);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;

		if (!(obj instanceof Time))
			return false;

		final Time other = (Time) obj;
		return this.milliseconds == other.milliseconds;
	}

	@Override
	public int compareTo(final Time other) {
		return Long.compare(this.milliseconds, other.milliseconds);
	}
}
