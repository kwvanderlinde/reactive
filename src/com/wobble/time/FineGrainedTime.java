package com.wobble.time;

/**
 * A non-flat representation of time.
 *
 * A FineGrainedTime extends Time with a "counter", which represents an
 * infinitesimal step forward in time. This is useful, for instance, to
 * discretize the flow of time within a single frame of an application.
 *
 * This non-flat time is ordered lexicographically.
 *
 * @author ken
 */
public final class FineGrainedTime implements Comparable<FineGrainedTime> {
	private final Time base;
	private final int counter;

	public FineGrainedTime() {
		this(new Time(), 0);
	}

	public FineGrainedTime(final Time base) {
		this(base, 0);
	}

	private FineGrainedTime(final Time base, final int counter) {
		this.base = base;
		this.counter = counter;
	}

	public FineGrainedTime getNext() {
		return new FineGrainedTime(this.base, this.counter + 1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;

		int result = 1;
		result = prime * result + base.hashCode();
		result = prime * result + Integer.hashCode(counter);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;

		if (!(obj instanceof FineGrainedTime))
			return false;

		final FineGrainedTime other = (FineGrainedTime) obj;
		return base.equals(other.base) && this.counter == other.counter;
	}

	@Override
	public int compareTo(final FineGrainedTime o) {
		final int baseResult = base.compareTo(o.base);
		if (baseResult != 0)
			return baseResult;

		final int counterResult = Integer.compare(this.counter, o.counter);
		return counterResult;
	}
}
