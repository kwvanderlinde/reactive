package com.wobble.reactive;

import java.util.List;

import com.wobble.time.FineGrainedTime;

public class Propagator {
    private FineGrainedTime lastPropagationTime;

    public Propagator() {
        this.lastPropagationTime = new FineGrainedTime();
    }

    public void propagateChanges(final FineGrainedTime time, final Dag graph,
            final EventSourceValueMap values) {
        if (time.compareTo(this.lastPropagationTime) <= 0) {
            throw new IllegalArgumentException(
                    "The given time must be greater than the last propagation time.");
        }

        // Save the value of each source node.
        values.stream().forEach(entry -> {
            // Permanently set the value of the event source.
            writeValueToEventSource(entry, time);
        });

        // Walk the DAG in topological order, invalidating and validating node
        // as we go.
        final List<ReactiveNode> nodes = graph.getTopologicalSort();
        for (final ReactiveNode node : nodes) {
            node.validate(time);
        }

        this.lastPropagationTime = time;
    }

    private <T> void writeValueToEventSource(final EventSourceValueMap.Entry<T> entry,
            final FineGrainedTime time) {
        final EventSource<T> source = entry.getSource();
        final T value = entry.getValue();
        source.setValue(time, value);
    }
}
