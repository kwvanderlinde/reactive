package com.wobble.reactive;

import com.wobble.time.FineGrainedTime;
import com.wobble.time.Time;

public class Scheduler {
    private final Context context;
    private final Propagator propagator;
    private FineGrainedTime currentTime;
    private final EventSourceValueMap values;

    public Scheduler(final Context context) {
        this.context = context;
        this.propagator = new Propagator();
        this.values = new EventSourceValueMap();
        // Be sure not to use time zero, in order to agree with the propagator.
        this.currentTime = new FineGrainedTime().getNext();
    }

    public <T> void schedule(final EventSource<T> changedNode, final T value) {
        // TODO Use strategies to determine how eagerly to begin a propagation
        // phase.
        if (values.contains(changedNode)) {
            this.propagateChanges();
        }

        values.put(changedNode, value);
    }

    public boolean propagationPending() {
        return !values.isEmpty();
    }

    public FineGrainedTime getCurrentTime() {
        return currentTime;
    }

    public void tick(final Time newTimeInMilliseconds) {
        final FineGrainedTime newTime = new FineGrainedTime(newTimeInMilliseconds);

        if (newTime.compareTo(currentTime) <= 0)
            throw new IllegalArgumentException("tick requires time to advance.");

        // Propagate changes using the old time.
        this.propagateChanges();

        this.currentTime = newTime;
    }

    private void propagateChanges() {
        this.propagator.propagateChanges(this.getCurrentTime(), this.context.getDag(), this.values);
        this.values.clear();
        this.currentTime = this.currentTime.getNext();
    }
}
