package com.wobble.reactive;

import com.wobble.time.FineGrainedTime;

public class Event<T> {
	private final FineGrainedTime time;
	private final T value;

	public Event(final FineGrainedTime time, final T value) {
		this.time = time;
		this.value = value;
	}

	public FineGrainedTime getTime() {
		return time;
	}

	public T getValue() {
		return value;
	}
}
