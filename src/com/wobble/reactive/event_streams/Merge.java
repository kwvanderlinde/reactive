package com.wobble.reactive.event_streams;

import java.util.ArrayList;
import java.util.Arrays;

import com.wobble.collections.Enumerable;
import com.wobble.reactive.Context;
import com.wobble.reactive.EventStream;
import com.wobble.time.FineGrainedTime;

/**
 * Merges two event streams.
 *
 * In case of simultaneous events, we follow the behaviour of Scala.react and
 * arbitrarily pick the first event stream.
 *
 * Together with Empty, makes EventStream a monoid.
 *
 * @author ken
 *
 * @param <T>
 */
public class Merge<T> extends EventStream<T> {
    private final Enumerable<EventStream<T>> streams;

    @SafeVarargs
    public Merge(final Context context, final EventStream<T>... streams) {
        super(context, streams);
        this.streams = Enumerable.of(new ArrayList<EventStream<T>>(Arrays.asList(streams)));
    }

    @Override
    protected Enumerable<T> generate(FineGrainedTime time) {
        return streams.bind(stream -> stream.at(time));
    }
}
