package com.wobble.reactive.event_streams;

import java.util.function.Function;

import com.wobble.collections.Enumerable;
import com.wobble.reactive.Behaviour;
import com.wobble.reactive.Context;
import com.wobble.reactive.EventStream;
import com.wobble.time.FineGrainedTime;

public class Apply<U, T> extends EventStream<T> {
    final Behaviour<Function<U, T>> behaviour;
    final EventStream<U> eventStream;

    public Apply(final Context context, final Behaviour<Function<U, T>> behaviour,
            final EventStream<U> eventStream) {
        super(context, eventStream);
        this.behaviour = behaviour;
        this.eventStream = eventStream;
    }

    @Override
    protected Enumerable<T> generate(final FineGrainedTime time) {
        return eventStream.at(time).map(behaviour.at(time)::apply);
    }
}
