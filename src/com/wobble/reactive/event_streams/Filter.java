package com.wobble.reactive.event_streams;

import java.util.function.Predicate;

import com.wobble.collections.Enumerable;
import com.wobble.reactive.Context;
import com.wobble.reactive.EventStream;
import com.wobble.time.FineGrainedTime;

public class Filter<T> extends EventStream<T> {
    private final EventStream<T> stream;
    private final Predicate<T> predicate;

    public Filter(final Context context, final EventStream<T> stream, final Predicate<T> predicate) {
        super(context, stream);
        this.stream = stream;
        this.predicate = predicate;
    }

    @Override
    protected Enumerable<T> generate(final FineGrainedTime time) {
        // Only generate an event if the underlying stream generated an event
        // and the predicate holds.
        return stream.at(time).filter(this.predicate);
    }
}
