package com.wobble.reactive.event_streams;

import java.util.function.Function;

import com.wobble.collections.Enumerable;
import com.wobble.reactive.Context;
import com.wobble.reactive.EventStream;
import com.wobble.time.FineGrainedTime;

public class Map<U, T> extends EventStream<T> {
    private final Function<U, T> mapper;
    private final EventStream<U> eventStream;

    public Map(final Context context, final Function<U, T> mapper, final EventStream<U> eventStream) {
        super(context, eventStream);
        this.mapper = mapper;
        this.eventStream = eventStream;
    }

    @Override
    protected Enumerable<T> generate(final FineGrainedTime time) {
        return eventStream.at(time).map(mapper::apply);
    }
}
