package com.wobble.reactive.event_streams;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.wobble.collections.Enumerable;
import com.wobble.reactive.Context;
import com.wobble.reactive.EventStream;
import com.wobble.time.FineGrainedTime;

public class Accumulate<T> extends EventStream<T> {
    private final T initialValue;
    private final EventStream<Function<T, T>> mapStream;

    public Accumulate(final Context context, final T initialValue, final EventStream<Function<T, T>> mapStream) {
        super(context, mapStream);
        this.initialValue = initialValue;
        this.mapStream = mapStream;
    }

    @Override
    protected Enumerable<T> generate(final FineGrainedTime time) {
        // Get new maps if available.
        final Enumerable<Function<T, T>> functions = mapStream.at(time);
        if (functions.isEmpty())
            return Enumerable.empty();

        final List<T> newValues = new ArrayList<>(functions.size());

        // Get the latest value of this.
        T accum = this.latestAtOrBefore(time).orElse(this.initialValue);

        // Fold over the functions, applying each one and collecting the results
        // into a list.
        for (final Function<T, T> function : functions) {
            accum = function.apply(accum);
            newValues.add(accum);
        }
        return Enumerable.of(newValues);
    }
}
