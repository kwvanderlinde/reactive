package com.wobble.reactive.event_streams;

import com.wobble.collections.Enumerable;
import com.wobble.reactive.Context;
import com.wobble.reactive.EventStream;
import com.wobble.time.FineGrainedTime;

/**
 * Never generates an event.
 *
 * Together with Merge, makes EventStream a monoid.
 *
 * @author ken
 *
 * @param <T>
 */
public class Empty<T> extends EventStream<T> {
    public Empty(final Context context) {
        super(context);
    }

    @Override
    protected Enumerable<T> generate(final FineGrainedTime time) {
        return Enumerable.empty();
    }
}
