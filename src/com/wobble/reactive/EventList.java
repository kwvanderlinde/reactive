package com.wobble.reactive;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.wobble.collections.Enumerable;
import com.wobble.collections.Utils;
import com.wobble.time.FineGrainedTime;

/**
 * A mapping from time intervals to values.
 *
 * Looking can be done for a precise point in time, or for the latest time
 * before a given time. Also, points can only be appended to the time line,
 * never prepended or inserted elsewhere.
 *
 * @author ken
 *
 * @param <T>
 */
public class EventList<T> {
    // Sorted on time to allow binary searching.
    // private final List<Event<T>> points;
    public static class MultiEvent<T> {
        private final FineGrainedTime time;
        private final Enumerable<T> values;

        public MultiEvent(final FineGrainedTime time, final Enumerable<T> values) {
            this.time = time;
            this.values = values;
        }

        public FineGrainedTime getTime() {
            return this.time;
        }

        public Enumerable<T> getValues() {
            return this.values;
        }
    }

    private final List<MultiEvent<T>> points;

    public EventList() {
        this.points = new ArrayList<MultiEvent<T>>();
    }

    /**
     * Adds a single event at the given time.
     *
     * @param time
     * @param value
     */
    public void add(final FineGrainedTime time, final T value) {
        this.addAll(time, Enumerable.singleton(value));
    }

    /**
     * Adds a set of events at the given time. These events are truly
     * simultaneous.
     *
     * @param time
     * @param values
     */
    public void addAll(final FineGrainedTime time, final Enumerable<T> values) {
        if (values.isEmpty()) {
            throw new IllegalArgumentException("An event occurrence must have values!");
        }

        if (!this.canAppendTime(time)) {
            throw new IllegalArgumentException("This time has come and gone!");
        }

        points.add(new MultiEvent<>(time, values));
    }

    /**
     * Retrieves the value precisely at time.
     *
     * @param time
     * @return
     */
    public Enumerable<T> get(final FineGrainedTime time) {
        final Optional<MultiEvent<T>> lowerBound = this.greatestLowerBound(time);

        if (!lowerBound.isPresent())
            return Enumerable.empty();

        final MultiEvent<T> event = lowerBound.get();
        if (!event.getTime().equals(time))
            return Enumerable.empty();

        return event.getValues();
    }

    /**
     * Retrieves the most recent value which comes before time.
     *
     * @param time
     * @return
     */
    public Enumerable<T> getMostRecent(final FineGrainedTime time) {
        final Optional<MultiEvent<T>> lowerBound = this.greatestLowerBound(time);

        if (!lowerBound.isPresent())
            return Enumerable.empty();

        return lowerBound.get().getValues();
    }

    private boolean canAppendTime(FineGrainedTime time) {
        if (points.isEmpty())
            return true;

        final FineGrainedTime lastTime = points.get(points.size() - 1).getTime();
        return lastTime.compareTo(time) < 0;
    }

    private Optional<MultiEvent<T>> greatestLowerBound(final FineGrainedTime time) {
        int index = Utils.binarySearch(this.points, event -> event.getTime().compareTo(time));
        if (index < 0) {
            // The time was not found exactly. index is the negative of one past
            // the least upper bound. Turn this into the greatest lower bound.
            final int insertionPoint = -index - 1;
            index = insertionPoint - 1;

            if (index < 0) {
                // The index is still negative because there is no earlier time.
                return Optional.empty();
            }
        }

        return Optional.of(this.points.get(index));
    }

    /**
     * Get the values in the given time range.
     *
     * @param start
     *            The start of the time range (inclusive).
     * @param end
     *            The end of the time range (exclusive).
     * @return The events within the given range of times.
     */
    public Enumerable<MultiEvent<T>> range(FineGrainedTime start, FineGrainedTime end) {
        if (start.compareTo(end) >= 0)
            return Enumerable.empty();

        // Find the first time in the map no less than the start time.
        int startIndex = Utils.binarySearch(this.points, event -> event.getTime().compareTo(start));
        if (startIndex < 0) {
            // The exact time doesn't exist. But startIndex is the negative of
            // one past the next time.
            startIndex = -startIndex - 1;
        }

        // Find the first time in the map no less than the end time.
        int endIndex = Utils.binarySearch(this.points, event -> event.getTime().compareTo(end));
        if (endIndex < 0) {
            // The exact time doesn't exist. But endIndex is the negative of one
            // past the next time.
            endIndex = -endIndex - 1;
        }

        return Enumerable.of(points.subList(startIndex, endIndex));
    }
}
