package com.wobble.reactive;

import com.wobble.time.FineGrainedTime;

public abstract class Behaviour<T> extends ReactiveNode {
	public Behaviour(Context context, EventStream<?>... dependencies) {
		super(context, dependencies);
	}

	public abstract T at(FineGrainedTime time);

	@Override
	protected void onValidate(FineGrainedTime time) {
		// Behaviours do not generally require validation, though they are
		// allowed validation if it helps.
	}
}
