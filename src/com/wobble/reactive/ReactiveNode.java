package com.wobble.reactive;

import java.util.Iterator;

import com.wobble.collections.Enumerable;
import com.wobble.collections.iterators.TimeStreamMergeIterator;
import com.wobble.time.FineGrainedTime;

public abstract class ReactiveNode {
    private final Enumerable<EventStream<?>> dependencies;
    private final Context context;
    private FineGrainedTime lastValidationTime;

    ReactiveNode(final Context context, final EventStream<?>... dependencies) {
        // Check that we are not mixing reactive DAGs from different contexts.
        for (final EventStream<?> dependency : dependencies) {
            if (dependency.getContext() != context) {
                throw new IllegalArgumentException(
                        "Cannot mix reactive graphs from different contexts.");
            }
        }

        this.dependencies = Enumerable.of(dependencies);
        this.context = context;
        this.lastValidationTime = new FineGrainedTime();
    }

    public Context getContext() {
        return context;
    }

    public Enumerable<EventStream<?>> getDependencies() {
        return this.dependencies;
    }

    public FineGrainedTime getLastValidationTime() {
        return this.lastValidationTime;
    }

    private void setLastValidationTime(FineGrainedTime lastValidationTime) {
        this.lastValidationTime = lastValidationTime;
    }

    protected Enumerable<FineGrainedTime> getCriticalTimesSinceLastValidation(
            final FineGrainedTime currentTime) {
        final FineGrainedTime lastValidation = this.getLastValidationTime();

        if (currentTime.compareTo(lastValidation) <= 0)
            throw new IllegalArgumentException("Cannot get critical times from the past.");

        // We don't want to return any times that have already been handled, so
        // start with the time just after the last validation.
        final FineGrainedTime start = lastValidation.getNext();

        // We do want to return the current time, if it applies. But
        // EventStream.getEventTimes does not include the end time, so use the
        // time just after the current time.
        final FineGrainedTime end = currentTime.getNext();

        if (start.compareTo(end) >= 0)
            return Enumerable.empty();

        return new Enumerable<FineGrainedTime>() {
            @Override
            public Iterator<FineGrainedTime> iterator() {
                return new TimeStreamMergeIterator(ReactiveNode.this.getDependencies().map(
                        node -> node.getEventTimes(start, end)));
            }
        };
    }

    public void validate(final FineGrainedTime time) {
        // All nodes should be up-to-date when validate is called.
        assert this.dependencies.map(ReactiveNode::getLastValidationTime).map(time::compareTo)
        .allMatch(compare -> compare <= 0);

        this.onValidate(time);
        this.setLastValidationTime(time);
    }

    protected abstract void onValidate(final FineGrainedTime time);
}
