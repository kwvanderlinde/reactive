package com.wobble.reactive;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class EventSourceValueMap implements Iterable<EventSourceValueMap.Entry<?>> {
	public class Entry<T> {
		private final EventSource<T> source;
		private final T value;

		public Entry(final EventSource<T> source, final T value) {
			this.source = source;
			this.value = value;
		}

		public EventSource<T> getSource() {
			return source;
		}

		public T getValue() {
			return value;
		}
	}

	final HashMap<Object, Object> impl;

	public EventSourceValueMap() {
		this.impl = new HashMap<Object, Object>();
	}

	public <T> void put(final EventSource<T> source, final T value) {
		this.impl.put(source, value);
	}

	public boolean isEmpty() {
		return impl.isEmpty();
	}

	public <T> T get(final EventSource<T> source) {
		// This cast is safe because users may only add values corresponding to
		// the source's type.
		@SuppressWarnings("unchecked")
		final T result = (T) this.impl.get(source);

		return result;
	}

	public <T> boolean contains(final EventSource<T> source) {
		return this.impl.containsKey(source);
	}

	void clear() {
		this.impl.clear();
	}

	@Override
	public Iterator<Entry<?>> iterator() {
		return new Iterator<Entry<?>>() {
			private final Iterator<Object> iterator = EventSourceValueMap.this.impl.keySet().iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			private <T> Entry<T> makeEntry(final EventSource<T> source) {
				return new Entry<T>(source, EventSourceValueMap.this.get(source));
			}

			@Override
			public Entry<?> next() {
				// The map is guaranteed to only contain EventSource<?>.
				EventSource<?> source = (EventSource<?>) iterator.next();
				return makeEntry(source);
			}
		};
	}

	@Override
	public Spliterator<Entry<?>> spliterator() {
		final Iterator<Entry<?>> it = this.iterator();
		final Spliterator<Entry<?>> split = Spliterators.spliterator(it, this.impl.size(), Spliterator.NONNULL);
		return split;
	}

	public Stream<Entry<?>> stream() {
		final boolean parallel = false;
		return StreamSupport.stream(this.spliterator(), parallel);
	}
}
