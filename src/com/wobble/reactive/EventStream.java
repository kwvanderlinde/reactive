package com.wobble.reactive;

import java.util.Optional;

import com.wobble.collections.Enumerable;
import com.wobble.reactive.EventList.MultiEvent;
import com.wobble.time.FineGrainedTime;

// TODO Implementations of EventStream should have an "iterator" of sorts for each dependency.
// This iterator will progress through a list of event until no more are currently available.
// The iterator must be potentially resumable. I.e., we ask the question "Are any more available currently?"
// If no, we stop for now. If yes, we consume the event and continue. Also, if none are currently available,
// we might check for a true end to the event stream. This is a replacement for explicitly working with lists
// returned from the query methods of event streams.
public abstract class EventStream<T> extends ReactiveNode {
    private final EventList<T> values;

    public EventStream(final Context context, final EventStream<?>... dependencies) {
        super(context, dependencies);
        this.values = new EventList<T>();
    }

    protected EventList<T> getValues() {
        return this.values;
    }

    public Enumerable<T> at(final FineGrainedTime time) {
        return this.getValues().get(time);
    }

    /**
     * Returns the latest occurrence no later than time. If no occurrence
     * exists, then empty is returned.
     *
     * @param time
     * @return
     */
    public Optional<T> latestAtOrBefore(final FineGrainedTime time) {
        final Enumerable<T> latestValues = this.getValues().getMostRecent(time);

        if (latestValues.isEmpty())
            return Optional.empty();

        return Optional.of(latestValues.last());
    }

    public Enumerable<FineGrainedTime> getEventTimes(final FineGrainedTime start,
            final FineGrainedTime end) {
        if (start.compareTo(end) > 0)
            throw new IllegalArgumentException("The start time cannot exceed the end time.");

        if (end.compareTo(this.getLastValidationTime().getNext()) > 0)
            throw new IllegalArgumentException("The time range extends into the future.");

        return this.getValues().range(start, end).map(MultiEvent::getTime);
    }

    @Override
    protected void onValidate(final FineGrainedTime time) {
        // Perform a validation step on all new times.
        for (final FineGrainedTime timePoint : this.getCriticalTimesSinceLastValidation(time)) {
            final Enumerable<T> values = this.generate(timePoint);
            if (values.isEmpty())
                continue;
            this.getValues().addAll(time, values);
        }
    }

    /**
     * Called when any dependencies of the event are invalidated.
     *
     * If the returned value is empty, no event is emitted. Otherwise, a new
     * event is emitted with the returned value.
     *
     * @return
     */
    protected abstract Enumerable<T> generate(final FineGrainedTime time);
}
