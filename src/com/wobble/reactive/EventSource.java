package com.wobble.reactive;

import com.wobble.collections.Enumerable;
import com.wobble.time.FineGrainedTime;

public class EventSource<T> extends EventStream<T> {
    public EventSource(Context context) {
        super(context);
    }

    public void push(final T value) {
        this.getContext().getScheduler().schedule(this, value);
    }

    /**
     * Source nodes never generate events in response to changing dependencies,
     * because they have no dependencies.
     */
    @Override
    protected Enumerable<T> generate(final FineGrainedTime time) {
        return Enumerable.empty();
    }

    void setValue(final FineGrainedTime time, final T value) {
        this.getValues().add(time, value);
    }
}
