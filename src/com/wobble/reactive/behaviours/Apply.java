package com.wobble.reactive.behaviours;

import java.util.function.Function;

import com.wobble.reactive.Behaviour;
import com.wobble.reactive.Context;
import com.wobble.time.FineGrainedTime;

/**
 * Allows applying a function-valued behaviour to a behaviour of the domain
 * type.
 *
 * Together with Lift, makes Behaviour an Applicative Functor.
 *
 * @author ken
 *
 * @param <T>
 */
public class Apply<U, T> extends Behaviour<T> {
    private final Behaviour<Function<U, T>> mapBehaviour;
    private final Behaviour<U> inputBehaviour;

    public Apply(final Context context, final Behaviour<Function<U, T>> mapBehaviour,
            final Behaviour<U> inputBehaviour) {
        super(context);
        this.mapBehaviour = mapBehaviour;
        this.inputBehaviour = inputBehaviour;
    }

    @Override
    public T at(final FineGrainedTime time) {
        return mapBehaviour.at(time).apply(inputBehaviour.at(time));
    }

}
