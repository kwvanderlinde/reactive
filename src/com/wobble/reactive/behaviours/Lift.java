package com.wobble.reactive.behaviours;

import com.wobble.reactive.Behaviour;
import com.wobble.reactive.Context;
import com.wobble.time.FineGrainedTime;

/**
 * Turns a single value into a behaviour.
 *
 * Together with Apply, makes Behaviour an Applicative Functor.
 *
 * @author ken
 *
 * @param <T>
 */
public class Lift<T> extends Behaviour<T> {
	private final T value;

	public Lift(final Context context, final T value) {
		super(context);
		this.value = value;
	}

	@Override
	public T at(final FineGrainedTime time) {
		return value;
	}

}
