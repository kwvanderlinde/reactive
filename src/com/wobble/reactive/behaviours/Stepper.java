package com.wobble.reactive.behaviours;

import com.wobble.reactive.Behaviour;
import com.wobble.reactive.Context;
import com.wobble.reactive.EventStream;
import com.wobble.time.FineGrainedTime;

public class Stepper<T> extends Behaviour<T> {
	private final T initialValue;
	private final EventStream<T> stream;

	public Stepper(final Context context, final T initialValue, final EventStream<T> stream) {
		super(context, stream);
		this.initialValue = initialValue;
		this.stream = stream;
	}

	@Override
	public T at(FineGrainedTime time) {
		return stream.latestAtOrBefore(time).orElse(this.initialValue);
	}

}
