package com.wobble.reactive.behaviours;

import java.util.function.Function;

import com.wobble.reactive.Behaviour;
import com.wobble.reactive.Context;
import com.wobble.time.FineGrainedTime;

public class Map<U, T> extends Behaviour<T> {
    private final Function<U, T> mapper;
    private final Behaviour<U> behaviour;

    public Map(final Context context, final Function<U, T> mapper, final Behaviour<U> behaviour) {
        super(context);
        this.mapper = mapper;
        this.behaviour = behaviour;
    }

    @Override
    public T at(final FineGrainedTime time) {
        return mapper.apply(behaviour.at(time));
    }
}
