package com.wobble.reactive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.wobble.collections.Enumerable;

public class Dag {
	private final Set<ReactiveNode> unsortedNodes;
	private final List<ReactiveNode> sortedNodes;

	public Dag() {
		this.unsortedNodes = new HashSet<>();
		this.sortedNodes = new ArrayList<>();
	}

	public void addNode(final ReactiveNode node) {
		// We can't add a node multiple times. This could ruin the topological
		// sorting.
		if (this.unsortedNodes.contains(node)) {
			throw new IllegalArgumentException(
					"Node is already a member of the DAG.");
		}

		// By design of the reactive library, there should be no way for a
		// dependency to be missing.
		final boolean haveAllDependencies = Enumerable.of(this.unsortedNodes).containsAll(node.getDependencies());
		if (!haveAllDependencies) {
			throw new IllegalArgumentException(
					"Node's dependencies are not all members of the DAG.");
		}

		this.unsortedNodes.add(node);
		this.sortedNodes.add(node);
	}

	public List<ReactiveNode> getTopologicalSort() {
		return Collections.unmodifiableList(sortedNodes);
	}
}
