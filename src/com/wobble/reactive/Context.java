package com.wobble.reactive;

import java.util.function.Function;
import java.util.function.Predicate;

import com.wobble.reactive.behaviours.Lift;
import com.wobble.reactive.behaviours.Stepper;
import com.wobble.reactive.event_streams.Accumulate;
import com.wobble.reactive.event_streams.Empty;
import com.wobble.reactive.event_streams.Filter;
import com.wobble.reactive.event_streams.Merge;

public class Context {
    private final Scheduler scheduler;
    private final Dag dag;

    public Context() {
        this.dag = new Dag();
        this.scheduler = new Scheduler(this);
    }

    public Scheduler getScheduler() {
        return this.scheduler;
    }

    public Dag getDag() {
        return dag;
    }

    // Event Combinators,

    private final <T extends ReactiveNode> T addNode(final T node) {
        this.getDag().addNode(node);
        return node;
    }

    public final <T> EventSource<T> eventSource() {
        return this.addNode(new EventSource<T>(this));
    }

    public final <T> EventStream<T> accumulate(final T initialValue,
            final EventStream<Function<T, T>> mapStream) {
        return this.addNode(new Accumulate<T>(this, initialValue, mapStream));
    }

    public final <U, T> EventStream<T> apply(
            final Behaviour<Function<U, T>> behaviour,
            final EventStream<U> stream) {
        return this.addNode(new com.wobble.reactive.event_streams.Apply<U, T>(
                this, behaviour, stream));
    }

    public final <T> EventStream<T> empty() {
        return this.addNode(new Empty<T>(this));
    }

    public final <T> EventStream<T> filter(final Predicate<T> predicate,
            final EventStream<T> stream) {
        return this.addNode(new Filter<T>(this, stream, predicate));
    }

    public final <U, T> EventStream<T> map(final Function<U, T> mapper,
            final EventStream<U> stream) {
        return this.addNode(new com.wobble.reactive.event_streams.Map<U, T>(this,
                mapper, stream));
    }

    @SafeVarargs
    public final <T> EventStream<T> merge(final EventStream<T>... eventStreams) {
        return this.addNode(new Merge<T>(this, eventStreams));
    }

    // Behaviour Combinators.

    public <U, T> Behaviour<T> apply(
            final Behaviour<Function<U, T>> mapBehaviour,
            final Behaviour<U> inputBehaviour) {
        return this.addNode(new com.wobble.reactive.behaviours.Apply<U, T>(this,
                mapBehaviour, inputBehaviour));
    }

    public <T> Behaviour<T> lift(final T value) {
        return this.addNode(new Lift<T>(this, value));
    }

    public <U, T> Behaviour<T> map(final Function<U, T> mapper,
            final Behaviour<U> behaviour) {
        return this.addNode(new com.wobble.reactive.behaviours.Map<U, T>(this,
                mapper, behaviour));
    }

    public <T> Behaviour<T> stepper(final T initialValue,
            final EventStream<T> stream) {
        return this.addNode(new Stepper<T>(this, initialValue, stream));
    }
}
