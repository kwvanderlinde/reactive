package com.wobble.collections.iterators;

import java.util.Iterator;
import java.util.function.Function;

public class MapIterator<T, U> implements Iterator<U> {
	private final Function<T, U> function;
	private final Iterator<T> iterator;

	public MapIterator(final Function<T, U> function, final Iterator<T> iterator) {
		this.function = function;
		this.iterator = iterator;
	}

	@Override
	public boolean hasNext() {
		return iterator.hasNext();
	}

	@Override
	public U next() {
		return function.apply(iterator.next());
	}
}
