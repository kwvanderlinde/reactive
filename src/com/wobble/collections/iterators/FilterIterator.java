package com.wobble.collections.iterators;

import java.util.Iterator;
import java.util.function.Predicate;

public class FilterIterator<T> implements Iterator<T> {
    private final Iterator<T> iterator;
    private final Predicate<T> predicate;
    private T next;
    private boolean hasNext;

    public FilterIterator(final Iterator<T> iterator, final Predicate<T> predicate) {
        this.iterator = iterator;
        this.predicate = predicate;
        this.findValue();
    }

    @Override
    public boolean hasNext() {
        return this.hasNext;
    }

    @Override
    public T next() {
        if (!this.hasNext())
            throw new IllegalStateException();

        final T value = this.next;
        this.findValue();
        return value;
    }

    private void findValue() {
        while (this.iterator.hasNext()) {
            final T next = this.iterator.next();
            if (this.predicate.test(next)) {
                this.next = next;
                this.hasNext = true;
                return;
            }
        }

        this.next = null;
        this.hasNext = false;
    }

}
