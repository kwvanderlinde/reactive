package com.wobble.collections.iterators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ConcatIterator<T> implements Iterator<T> {
	private final List<Iterator<T>> iterators;
	private int index;

	@SafeVarargs
	public ConcatIterator(Iterator<T>... iterators) {
		this(Arrays.asList(iterators));
	}

	public ConcatIterator(List<Iterator<T>> iterators) {
		this.iterators = new ArrayList<Iterator<T>>(iterators);
		this.index = 0;
	}

	@Override
	public boolean hasNext() {
		this.advanceIndex();
		return this.index < iterators.size();
	}

	@Override
	public T next() {
		if (!hasNext())
			throw new IllegalStateException();

		return this.iterators.get(index).next();
	}

	/**
	 * Set `index` to the first index of a non-empty iterator, or else
	 * iterators.size();
	 */
	private void advanceIndex() {
		final int numberOfIterators = iterators.size();
		while (index < numberOfIterators && !iterators.get(index).hasNext()) {
			++index;
		}
	}
}
