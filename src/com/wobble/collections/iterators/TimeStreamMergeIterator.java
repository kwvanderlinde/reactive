package com.wobble.collections.iterators;

import java.util.Iterator;
import java.util.List;

import com.wobble.collections.Enumerable;
import com.wobble.time.FineGrainedTime;

public class TimeStreamMergeIterator implements Iterator<FineGrainedTime> {
    private static class CachedIterator<T> {
        private final Iterator<T> iterator;
        private T value;
        private boolean hasValue;

        public CachedIterator(final Iterator<T> iterator) {
            this.iterator = iterator;
            this.moveToNextValue();
        }

        public boolean hasValue() {
            return this.hasValue;
        }

        public T getValue() {
            if (!this.hasValue())
                throw new IllegalStateException();

            return this.value;
        }

        public void advance() {
            if (!this.hasValue())
                throw new IllegalStateException();

            this.moveToNextValue();
        }

        private void moveToNextValue() {
            if (iterator.hasNext()) {
                this.value = iterator.next();
                this.hasValue = true;
            }
            else {
                this.value = null;
                this.hasValue = false;
            }
        }
    }

    private final List<CachedIterator<FineGrainedTime>> iterators;

    public TimeStreamMergeIterator(Enumerable<Enumerable<FineGrainedTime>> iteratorList) {
        this.iterators = iteratorList.map(Enumerable::iterator).map(CachedIterator<FineGrainedTime>::new).toList();

        // Remove exhausted iterators.
        final Iterator<CachedIterator<FineGrainedTime>> iterator = this.iterators.iterator();
        while (iterator.hasNext()) {
            final CachedIterator<FineGrainedTime> cachedIterator = iterator.next();
            if (!cachedIterator.hasValue()) {
                iterator.remove();
            }
        }
    }

    @Override
    public boolean hasNext() {
        return !this.iterators.isEmpty();
    }

    @Override
    public FineGrainedTime next() {
        if (!this.hasNext())
            throw new IllegalStateException();

        // Find the iterator with the least value.
        // TODO Do a better job than a plain loop!
        CachedIterator<FineGrainedTime> minIterator = iterators.get(0);
        for(CachedIterator<FineGrainedTime> iterator : iterators) {
            if (iterator.getValue().compareTo(minIterator.getValue()) < 0)
                minIterator = iterator;
        }

        // Grab the time from the minimum iterator, and advance it.
        final FineGrainedTime time = minIterator.getValue();
        minIterator.advance();
        if (!minIterator.hasValue())
            iterators.remove(minIterator);
        return time;
    }
}
