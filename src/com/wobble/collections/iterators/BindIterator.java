package com.wobble.collections.iterators;

import java.util.Iterator;
import java.util.function.Function;

import com.wobble.collections.Enumerable;

public class BindIterator<T, U> implements Iterator<U> {
	private final Function<T, Enumerable<U>> function;
	private final Iterator<T> iterator;
	private Iterator<U> current;

	public BindIterator(final Function<T, Enumerable<U>> function, final Iterator<T> iterator) {
		this.function = function;
		this.iterator = iterator;
		this.current = new EmptyIterator<>();
	}

	@Override
	public boolean hasNext() {
		this.findIteratorPosition();
		return this.current.hasNext();
	}

	@Override
	public U next() {
		if (!this.hasNext())
			throw new IllegalStateException();

		return this.current.next();
	}

	private void findIteratorPosition() {
		while (!current.hasNext()) {
			if (!iterator.hasNext())
				return;

			// Get the next iterator.
			current = function.apply(iterator.next()).iterator();
		}
	}

}
