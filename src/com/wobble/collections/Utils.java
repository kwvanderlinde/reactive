package com.wobble.collections;

import java.util.List;
import java.util.function.Function;

public class Utils {
	public static <T> int binarySearch(List<? extends T> list, Function<? super T, Integer> comparer) {
		int min = 0;
		int max = list.size() - 1;
		while (min <= max) {
			final int median = (min + max) / 2;
			final T event = list.get(median);
			final int compareResult = comparer.apply(event);

			if (compareResult < 0)
				min = median + 1;
			else if (compareResult > 0)
				max = median - 1;
			else
				return median; // found time precisely
		}

		// The time was not found. But min now points at the insertion point
		// (i.e., at the least upper bound). Return the negative of one past the
		// insertion point, as in Collections::binarySearch.

		return -(min + 1);
	}
}
