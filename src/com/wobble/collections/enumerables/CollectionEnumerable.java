package com.wobble.collections.enumerables;

import java.util.Collection;
import java.util.Iterator;

import com.wobble.collections.Enumerable;

public class CollectionEnumerable<T> implements Enumerable<T> {
    private final Collection<T> collection;

    public CollectionEnumerable(Collection<T> collection) {
        this.collection = collection;
    }

    @Override
    public boolean contains(final T value) {
        return collection.contains(value);
    }

    @Override
    public int size() {
        return collection.size();
    }

    @Override
    public Iterator<T> iterator() {
        return collection.iterator();
    }

}
