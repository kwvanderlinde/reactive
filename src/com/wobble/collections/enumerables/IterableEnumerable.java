package com.wobble.collections.enumerables;

import java.util.Iterator;

import com.wobble.collections.Enumerable;

public class IterableEnumerable<T> implements Enumerable<T> {
    private final Iterable<T> iterable;

    public IterableEnumerable(final Iterable<T> iterable) {
        this.iterable = iterable;
    }

    @Override
    public Iterator<T> iterator() {
        return iterable.iterator();
    }

}
