package com.wobble.collections.enumerables;

import java.util.Iterator;
import java.util.List;

import com.wobble.collections.Enumerable;

public class ListEnumerable<T> implements Enumerable<T> {
    private final List<T> list;

    public ListEnumerable(List<T> list) {
        this.list = list;
    }

    @Override
    public boolean contains(final T value) {
        return list.contains(value);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public Iterator<T> iterator() {
        return list.iterator();
    }

    @Override
    public T get(int index) {
        return list.get(index);
    }
}
