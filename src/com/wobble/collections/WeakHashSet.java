package com.wobble.collections;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

public class WeakHashSet<T> extends AbstractSet<T> implements Set<T> {
	private final static Object commonValue = new Object();

	private final WeakHashMap<T, Object> impl;
	
	public WeakHashSet() {
		this.impl = new WeakHashMap<T, Object>();
	}

	public WeakHashSet(int initialCapacity) {
		this.impl = new WeakHashMap<T, Object>(initialCapacity);
	}

	public WeakHashSet(int initialCapacity, float loadFactor) {
		this.impl = new WeakHashMap<T, Object>(initialCapacity, loadFactor);
	}
	
	@Override
	public int size() {
		return impl.size();
	}

	@Override
	public boolean contains(Object o) {
		return impl.containsKey(o);
	}

	@Override
	public Iterator<T> iterator() {
		return impl.keySet().iterator();
	}

	@Override
	public boolean add(T value) {
		return impl.put(value, commonValue) == null;
	}

	@Override
	public boolean remove(Object o) {
		return impl.remove(o) == null;
	}

	@Override
	public void clear() {
		impl.clear();
	}
}
