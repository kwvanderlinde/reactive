package com.wobble.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;

import com.wobble.collections.enumerables.CollectionEnumerable;
import com.wobble.collections.enumerables.IterableEnumerable;
import com.wobble.collections.enumerables.ListEnumerable;
import com.wobble.collections.iterators.BindIterator;
import com.wobble.collections.iterators.ConcatIterator;
import com.wobble.collections.iterators.EmptyIterator;
import com.wobble.collections.iterators.FilterIterator;
import com.wobble.collections.iterators.MapIterator;

public interface Enumerable<T> extends Iterable<T> {
    public class EmptySequenceException extends RuntimeException {
        private static final long serialVersionUID = 1757899435174515428L;

        public EmptySequenceException() {
        }
    }

    @SafeVarargs
    public static <T> Enumerable<T> of(final T... values) {
        return Enumerable.of(Arrays.asList(values));
    }

    public static <T> Enumerable<T> of(final Iterable<T> iterable) {
        if (iterable instanceof Collection<?>)
            return Enumerable.of((Collection<T>)iterable);

        return new IterableEnumerable<T>(iterable);
    }

    public static <T> Enumerable<T> of(final Collection<T> collection) {
        if (collection instanceof List<?>)
            return Enumerable.of((List<T>)collection);

        return new CollectionEnumerable<T>(collection);
    }

    public static <T> Enumerable<T> of(final List<T> list) {
        return new ListEnumerable<T>(list);
    }

    public static <T> Enumerable<T> singleton(final T value) {
        return Enumerable.of(Collections.singletonList(value));
    }

    public static <T> Enumerable<T> empty() {
        return new Enumerable<T>() {
            @Override
            public Iterator<T> iterator() {
                return new EmptyIterator<>();
            }
        };
    }

    default List<T> toList() {
        final List<T> list = new ArrayList<T>();
        for (final T value : this) {
            list.add(value);
        }
        return list;
    }

    default <A> A foldLeft(final A initial, final BiFunction<A, T, A> func) {
        A result = initial;
        for (final T value : this) {
            result = func.apply(result, value);
        }
        return result;
    }

    default T reduce(final BinaryOperator<T> func) {
        final Iterator<T> iterator = this.iterator();
        if (!iterator.hasNext())
            throw new EmptySequenceException();

        T result = iterator.next();
        while (iterator.hasNext()) {
            final T value = iterator.next();
            result = func.apply(result, value);
        }
        return result;
    }

    default boolean allMatch(final Predicate<T> predicate) {
        for (final T value : this) {
            if (!predicate.test(value))
                return false;
        }

        return true;
    }

    default boolean anyMatch(final Predicate<T> predicate) {
        for (final T value : this) {
            if (predicate.test(value))
                return true;
        }

        return false;
    }

    default boolean noneMatch(final Predicate<T> predicate) {
        return this.anyMatch(predicate.negate());
    }

    default boolean isEmpty() {
        return !this.iterator().hasNext();
    }

    default Enumerable<T> concat(final Enumerable<T> other) {
        return new Enumerable<T>() {
            @Override
            public Iterator<T> iterator() {
                return new ConcatIterator<>(this.iterator(), other.iterator());
            }
        };
    }

    default boolean contains(final T target) {
        for (final T value : this) {
            if (value.equals(target))
                return true;
        }
        return false;
    }

    default boolean contains(final T target, final BiPredicate<T, T> comparer) {
        for (final T value : this) {
            if (comparer.test(value, target))
                return true;
        }
        return false;
    }

    default boolean containsAll(final Enumerable<? extends T> collection) {
        for (final T value : collection) {
            if (!this.contains(value))
                return false;
        }
        return true;
    }

    default int size() {
        int count = 0;
        for (final T value : this) {
            ++count;
        }
        return count;
    }

    default int count(final Predicate<T> predicate) {
        int count = 0;
        for (final T value : this) {
            if (predicate.test(value))
                ++count;
        }
        return count;
    }

    default T get(int index) {
        for (final T value : this) {
            if (index == 0)
                return value;

            --index;
        }

        throw new IndexOutOfBoundsException();
    }

    default T getOrElse(int index, final T defaultValue) {
        if (index < 0)
            return defaultValue;

        if (this instanceof List<?>) {
            @SuppressWarnings("unchecked")
            final List<T> list = (List<T>) this;

            if (index >= list.size())
                return defaultValue;

            return list.get(index);
        }

        for (final T value : this) {
            if (index == 0)
                return value;

            --index;
        }
        return defaultValue;
    }

    default T first() {
        return this.get(0);
    }

    default T firstOrElse(final T defaultValue) {
        return this.getOrElse(0, defaultValue);
    }

    default T last() {
        return this.get(this.size() - 1);
    }

    default T lastOrElse(final T defaultValue) {
        return this.getOrElse(this.size() - 1, defaultValue);
    }

    default <U> Enumerable<U> map(final Function<T, U> function) {
        return new Enumerable<U>() {
            @Override
            public Iterator<U> iterator() {
                return new MapIterator<>(function, Enumerable.this.iterator());
            }
        };
    }

    default <U> Enumerable<U> bind(final Function<T, Enumerable<U>> function) {
        return new Enumerable<U>() {
            @Override
            public Iterator<U> iterator() {
                return new BindIterator<T, U>(function, Enumerable.this.iterator());
            }
        };
    }

    default Enumerable<T> filter(final Predicate<T> predicate) {
        return new Enumerable<T>() {
            @Override
            public Iterator<T> iterator() {
                return new FilterIterator<T>(Enumerable.this.iterator(), predicate);
            }
        };
    }
}
