The Wobble Reactive Library (WRL)
=================================

What is it?
-----------

The WRL is an implementation of Reactive Programming for the java
language. It is based on the implementation of Functional Reactive
Programming described in the 2012 paper "Deprecating the Observer
Pattern with Scala.React" by Igno Maier and Martin Odersky. The puporse
of Reactive Programming is the automatic and consistent propagation of
information. Alternatively, imagine that, instead of updating values
using hand-written loops (or some alternative), we instead expect values
to be updated automatically based on some dependencies. This is the
essence of Reactive Programming.

Who is it for?
--------------

This library can be applied to any number of libraries or
applications. The most obvious example is for capturing user input from
a mouse or keyboard, and updating the domain model accordingly. However,
Reactive Programming is useful for general communication between
components, and is good at reducing the coupling between components.

Where is it?
------------

The library is hosted as a Git repository on
[Bitbucket](https://bitbucket.org/kwvanderlinde/reactive). There is no
official distribution, just the source code. In the future, a JAR file
or Maven distribution may be made available.

Documentation
-------------

There is no official documentation, though the components map directly
to the concepts presented in the aforemention paper.